<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ショッピングカート</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link rel="stylesheet" href="css/cart.css">
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-md-7 col-sm-12">
				<table class="table">
					<thead>
						<tr>
							<th scope="col">#</th>
							<th scope="col">商品</th>
							<th scope="col">価格</th>
							<th scope="col">消去</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="idb" items="${cart}" varStatus="status">
							<tr>
								<th scope="row">${status.index + 1}</th>
								<td>
									<div class="row">
										<div class="col-6">
											<img src="img/${idb.fileName}">
										</div>
										<div class="col-6">
											<p>${idb.brandDataBeans.name}</p>
											<p>${idb.name}</p>
											<p>${idb.sizeDataBeans.size}</p>
										</div>
									</div>
								</td>
								<td>¥${idb.price}+tax</td>
								<td><a href="DeleteItem?id=${idb.id}&sizeId=${idb.sizeDataBeans.id}">×</a></td>
							</tr>
						</c:forEach>
						<tr>
							<td></td>
							<td>
								<div class="total">
									<p>合計</p>
									<p>送料</p>
									<p>消費税</p>
									<p class="p">ご注文合計</p>
								</div>
							</td>
							<td>
								<div class="total">
									<p>¥${totalPrice}</p>
									<p>¥0</p>
									<p>¥${tax}</p>
									<p class="p">¥${totalPrice + tax}</p>
								</div>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="col-md-5 col-sm-12">
				<div class="bbb">
					<a href="Buy"><p class="buy">購入する</p></a>
					<a href="Index"><p class="buy2">買い物を続ける</p></a>
				</div>
			</div>
		</div>
	</div>
</body>
</html>