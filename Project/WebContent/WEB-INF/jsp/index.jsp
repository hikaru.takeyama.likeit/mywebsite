<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>My EC Site（仮）</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link rel="stylesheet" href="css/index.css">
</head>
<body>


	<div class="container-fluid">
		<div class="row">
			<div class="col-2 left">
				<p>Category</p>
				<ul>
					<c:forEach var="cdb" items="${cdbList}">
						<li><a href="Index?cid=${cdb.id}">${cdb.name}</a></li>
					</c:forEach>
				</ul>
				<p>Brand</p>
				<ul>
					<c:forEach var="bdb" items="${bdbList}">
						<li><a href="Index?bid=${bdb.id}">${bdb.name}</a></li>
					</c:forEach>
				</ul>
			</div>
			<div class="col-8">
				<div class="row area2">
					<c:forEach var="idb" items="${idbList}">
						<div class="col-lg-3 col-md-6 col-sm-12 area3 item">
							<a href="Item?id=${idb.id}"><img src="img/${idb.fileName}"></a>
							<p class="item-brand">${idb.brandDataBeans.name}</p>
							<p class="item-name">${idb.name}</p>
							<p class="item-price">¥${idb.price}+tax</p>
						</div>
					</c:forEach>
				</div>
			</div>
			<div class="col-2 right">
				<p>並び替え</p>
				<ul>
					<li><a class="sort-link" href="Index?sortType=1">新着順</a></li>
					<li><a class="sort-link" href="Index">安い順</a></li>
					<li><a class="sort-link" href="Index">高い順</a></li>
				</ul>
			</div>
		</div>
	</div>


</body>
</html>