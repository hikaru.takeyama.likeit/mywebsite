<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>商品詳細</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link rel="stylesheet" href="css/itemdetail.css">
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-md-7 col-sm-12">
				<img  src="img/${idb.fileName}">
			</div>
			<div class="col-md-5 col-sm-12">
				<div>
					<p>${idb.brandDataBeans.name}</p>
					<h1>${idb.name}</h1>
					<p>Price: ¥${idb.price}+tax</p>
				</div>
				<div class="item-detail">
					<p>${idb.detail}</p>
				</div>
				<div>
					<form action="ItemAdd" method="post">
						<p>size</p>
						<select name="sizeId">
							<c:forEach var="isdb" items="${isdbList}">
								<option value="${isdb.sizeDataBeans.id}">${isdb.sizeDataBeans.size}</option>
							</c:forEach>
						</select>
						<input type="hidden" name="id" value="${idb.id}">
						<input type="submit" value="ADD TO CART">
					</form>
				</div>
			</div>
		</div>
	</div>
</body>
</html>