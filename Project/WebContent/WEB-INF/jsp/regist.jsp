<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>登録</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link rel="stylesheet" href="css/formstyle.css">
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-4">
				<h1>アカウントの登録</h1>
				<form>
					<p class="label">名前</p>
					<input class="inp" type="text" name="loginid">
					<p class="label">住所</p>
					<input class="inp" type="text" name="adress">
					<p class="label">ログインID</p>
					<input class="inp" type="text" name="loginid">
					<p class="label">パスワード</p>
					<input class="inp" type="password" name="password">
					<p class="label">確認</p>
					<input class="inp" type="password" name="password2">
					<input class="subm" type="submit" value="登録">
				</form>
			</div>
		</div>
	</div>
</body>
</html>