<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>商品追加</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<link rel="stylesheet" href="css/formstyle.css">
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-4">
				<h1>商品の新規作成</h1>
				<form action="ItemCreate" method="post" enctype="multipart/form-data">
					<p class="label">商品名</p>
					<input class="inp" type="text" name="name">
					<p class="label">商品詳細</p>
					<textarea class="inp" name="detail" rows="4"></textarea>
					<p class="label">価格</p>
					<input class="inp" type="text" name="price">
					<p class="label">画像ファイル</p>
					<input class="inp" type="file" name="file">
					<p class="label">ブランド</p>
					<select class="inp" name="brand">
						<c:forEach var="brand" items="${brandList}">
							<option value="${brand.id}">${brand.name}</option>
						</c:forEach>
					</select>
					<p class="label">カテゴリー</p>
					<select class="inp" name="category">
						<c:forEach var="category" items="${categoryList}">
							<option value="${category.id}">${category.name}</option>
						</c:forEach>
					</select>
					<p class="label">サイズ</p>
					<c:forEach var="size" items="${sizeList}">
						<input type="checkbox" name="size" value="${size.id}"> ${size.size}
					</c:forEach>
					<p class="label">在庫数</p>
					<input class="inp" type="text" name="stock">
					<input class="subm" type="submit" value="作成">
					<br><br>
					<a class="label" href="ItemManagement">戻る</a>
				</form>
			</div>
		</div>
	</div>
</body>
</html>