<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>商品更新</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<link rel="stylesheet" href="css/formstyle.css">
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-4">
				<h1>商品の詳細</h1>
				<div class="imgplace">
					<img src="img/${idb.fileName}">
				</div>
				<p class="label">サイズ（在庫数）</p>
				<c:forEach var="isdb" items="${isdbList}">
					<div class="label-content">
						<p>${isdb.sizeDataBeans.size}（${isdb.stock}）</p>
					</div>
				</c:forEach>
				<p class="label">商品名</p>
				<div class="label-content">
					<p>${idb.name}</p>
				</div>
				<p class="label">商品詳細</p>
				<textarea readonly class="inp" name="detail" rows="4">${idb.detail}</textarea>
				<p class="label">価格</p>
				<div class="label-content">
					<p>${idb.price}</p>
				</div>
				<p class="label">ブランド</p>
				<div class="label-content">
					<p>${idb.brandDataBeans.name}</p>
				</div>
				<p class="label">カテゴリー</p>
				<div class="label-content">
					<p>${idb.categoryDataBeans.name}</p>
				</div>
				<a class="label" href="ItemManagement">戻る</a>
			</div>
		</div>
	</div>
</body>
</html>