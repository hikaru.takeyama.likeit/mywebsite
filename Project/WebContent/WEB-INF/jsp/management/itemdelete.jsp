<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>商品削除</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<link rel="stylesheet" href="css/formstyle.css">
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-4">
				<h1>商品の削除</h1>
				<div class="imgplace">
				<img src="img/${idb.fileName}">
				</div>
				<form action="ItemDelete" method="post">
					<p class="label">商品名</p>
					<div class="label-content">
						<p>${idb.name}</p>
					</div>
					<input type="hidden" name="id" value="${idb.id}">
					<input class="subm" type="submit" value="削除">
					<br><br>
					<a class="label" href="ItemManagement">戻る</a>
				</form>
			</div>
		</div>
	</div>
</body>
</html>