<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>商品管理画面</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>
<body>
	<br><br><br><br>
	<a href="ItemCreate">新規登録</a>
	<br><br><br><br>
	<p>商品一覧</p>
	<br><br>
	<table class="table">
		<thead>
			<tr>
				<th scope="col">#</th>
				<th scope="col">商品名</th>
				<th scope="col">値段</th>
				<th scope="col">カテゴリー</th>
				<th scope="col">ブランド</th>
				<th scope="col">操作</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach var="item" items="${itemList}">
				<tr>
					<th scope="row">${item.id}</th>
					<td>${item.name}</td>
					<td>${item.price}</td>
					<td>${item.categoryDataBeans.name}</td>
					<td>${item.brandDataBeans.name}</td>
					<td><a href="ItemDetail?id=${item.id}">詳細</a> / <a href="ItemDelete?id=${item.id}">消去</a></td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</body>
</html>