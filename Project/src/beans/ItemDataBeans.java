package beans;

import dao.ItemDAO;
import dao.ItemSizeDAO;

public class ItemDataBeans {
	private int id;
	private String name;
	private String detail;
	private int price;
	private String fileName;
	private int categoryId;
	private int brandId;
	private int sizeId;
	private int stock;


	// 結合Beans
	public CategoryDataBeans getCategoryDataBeans() {
		CategoryDataBeans cdb = ItemDAO.getCategoryDataBeansByCategoryId(this.categoryId);
		return cdb;
	}

	public BrandDataBeans getBrandDataBeans() {
		BrandDataBeans bdb = ItemDAO.getBrandDataBeansByCategoryId(this.brandId);
		return bdb;
	}

	public SizeDataBeans getSizeDataBeans() {
		SizeDataBeans sdb = ItemSizeDAO.getSizeDataBeansBySizeId(sizeId);
		return sdb;
	}


//	create_date未実装

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDetail() {
		return detail;
	}
	public void setDetail(String detail) {
		this.detail = detail;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public int getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}
	public int getBrandId() {
		return brandId;
	}
	public void setBrandId(int brandId) {
		this.brandId = brandId;
	}
	public int getSizeId() {
		return sizeId;
	}
	public void setSizeId(int sizeId) {
		this.sizeId = sizeId;
	}
	public int getStock() {
		return stock;
	}
	public void setStock(int stock) {
		this.stock = stock;
	}

}
