package beans;

import dao.ItemSizeDAO;

public class ItemSizeDataBeans {
	private int id;
	private int itemId;
	private int sizeId;
	private int stock;

	public SizeDataBeans getSizeDataBeans() {
		SizeDataBeans sdb = ItemSizeDAO.getSizeDataBeansBySizeId(sizeId);
		return sdb;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getItemId() {
		return itemId;
	}
	public void setItemId(int itemId) {
		this.itemId = itemId;
	}
	public int getSizeId() {
		return sizeId;
	}
	public void setSizeId(int sizeId) {
		this.sizeId = sizeId;
	}
	public int getStock() {
		return stock;
	}
	public void setStock(int stock) {
		this.stock = stock;
	}
}
