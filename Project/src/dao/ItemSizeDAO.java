package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import beans.ItemSizeDataBeans;
import beans.SizeDataBeans;

public class ItemSizeDAO {

	public static ArrayList<ItemSizeDataBeans> getItemSizeDataById(int id) {
		Connection con = null;
		PreparedStatement st = null;
		ArrayList<ItemSizeDataBeans> isdbList = new ArrayList<ItemSizeDataBeans>();

		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("SELECT * FROM item_size WHERE item_id = ?");
			st.setInt(1, id);
			ResultSet rs = st.executeQuery();
			while(rs.next()) {
				ItemSizeDataBeans isdb = new ItemSizeDataBeans();
				isdb.setId(rs.getInt("id"));
				isdb.setItemId(rs.getInt("item_id"));
				isdb.setSizeId(rs.getInt("size_id"));
				isdb.setStock(rs.getInt("stock"));
				isdbList.add(isdb);
			}
		} catch(SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return isdbList;
	}

	public static SizeDataBeans getSizeDataBeansBySizeId(int sizeId) {
		Connection con = null;
		PreparedStatement st = null;
		SizeDataBeans sdb = new SizeDataBeans();
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("SELECT * FROM size WHERE id = ?");
			st.setInt(1, sizeId);
			ResultSet rs = st.executeQuery();
			if (rs.next()) {
				sdb.setId(rs.getInt("id"));
				sdb.setSize(rs.getString("size"));
			}
		} catch(SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return sdb;
	}

	public static int getStockByItemIdAndSizeId (int itemId, int sizeId) {
		Connection con = null;
		PreparedStatement st = null;
		int stock = -1;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("SELECT * FROM item_size WHERE item_id = ? AND size_id = ?");
			st.setInt(1, itemId);
			st.setInt(2, sizeId);
			ResultSet rs = st.executeQuery();
			if (rs.next()) {
				stock = rs.getInt("stock");
			}
		} catch(SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return stock;
	}

	public static void updateStockByItemIdAndSizeId (int itemId, int sizeId) {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("UPDATE item_size SET stock = stock - 1 WHERE item_id = ? AND size_id = ?");
			st.setInt(1, itemId);
			st.setInt(2, sizeId);
			st.executeUpdate();

		} catch(SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

}
