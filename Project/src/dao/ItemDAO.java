package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import beans.BrandDataBeans;
import beans.CategoryDataBeans;
import beans.ItemDataBeans;
import beans.SizeDataBeans;

public class ItemDAO {

	public static ArrayList<BrandDataBeans> getBrandData() {
		Connection con = null;
		Statement st = null;
		ArrayList<BrandDataBeans> brandList = new ArrayList<BrandDataBeans>();

		try {
			String sql = "SELECT * FROM brand";
			con = DBManager.getConnection();
			st = con.createStatement();
			ResultSet rs = st.executeQuery(sql);
			while(rs.next()) {
				BrandDataBeans brand = new BrandDataBeans();
				brand.setId(rs.getInt("id"));
				brand.setName(rs.getString("name"));
				brandList.add(brand);
			}

		} catch (SQLException e) {
			e.fillInStackTrace();
		} finally {
			 try {
                 con.close();
             } catch (SQLException e) {
                 e.printStackTrace();
             }
		}
		return brandList;
	}

	public static BrandDataBeans getBrandDataBeansByCategoryId(int brandId) {
		Connection con = null;
		PreparedStatement st = null;
		BrandDataBeans bdb = new BrandDataBeans();
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("SELECT * FROM brand WHERE id = ?");
			st.setInt(1, brandId);
			ResultSet rs = st.executeQuery();
			if (rs.next()) {
				bdb.setId(rs.getInt("id"));
				bdb.setName(rs.getString("name"));
			}
		} catch(SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return bdb;
	}

	public static ArrayList<CategoryDataBeans> getCategoryData() {
		Connection con = null;
		Statement st = null;
		ArrayList<CategoryDataBeans> categoryList = new ArrayList<CategoryDataBeans>();

		try {
			String sql = "SELECT * FROM category";
			con = DBManager.getConnection();
			st = con.createStatement();
			ResultSet rs = st.executeQuery(sql);
			while(rs.next()) {
				CategoryDataBeans category = new CategoryDataBeans();
				category.setId(rs.getInt("id"));
				category.setName(rs.getString("name"));
				categoryList.add(category);
			}

		} catch (SQLException e) {
			e.fillInStackTrace();
		} finally {
			 try {
                 con.close();
             } catch (SQLException e) {
                 e.printStackTrace();
             }
		}
		return categoryList;
	}

	public static CategoryDataBeans getCategoryDataBeansByCategoryId(int categoryId) {
		Connection con = null;
		PreparedStatement st = null;
		CategoryDataBeans cdb = new CategoryDataBeans();
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("SELECT * FROM category WHERE id = ?");
			st.setInt(1, categoryId);
			ResultSet rs = st.executeQuery();
			if (rs.next()) {
				cdb.setId(rs.getInt("id"));
				cdb.setName(rs.getString("name"));
			}
		} catch(SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return cdb;
	}

 	public static ArrayList<SizeDataBeans> getSizeData() {
		Connection con = null;
		Statement st = null;
		ArrayList<SizeDataBeans> sizeList = new ArrayList<SizeDataBeans>();

		try {
			String sql = "SELECT * FROM size";
			con = DBManager.getConnection();
			st = con.createStatement();
			ResultSet rs = st.executeQuery(sql);
			while(rs.next()) {
				SizeDataBeans size = new SizeDataBeans();
				size.setId(rs.getInt("id"));
				size.setSize(rs.getString("size"));
				sizeList.add(size);
			}

		} catch (SQLException e) {
			e.fillInStackTrace();
		} finally {
			 try {
                 con.close();
             } catch (SQLException e) {
                 e.printStackTrace();
             }
		}
		return sizeList;

	}

	public static ArrayList<ItemDataBeans> getAllItemData(){
		Connection con = null;
		Statement st = null;
		ArrayList<ItemDataBeans> itemList = new ArrayList<ItemDataBeans>();

		try {
			String sql = "select * from item";
			con = DBManager.getConnection();
			st = con.createStatement();
			ResultSet rs = st.executeQuery(sql);
			while(rs.next()) {
				ItemDataBeans item = new ItemDataBeans();
				item.setId(rs.getInt("id"));
				item.setName(rs.getString("name"));
				item.setDetail(rs.getString("detail"));
				item.setPrice(rs.getInt("price"));
				item.setFileName(rs.getString("file_name"));
				item.setCategoryId(rs.getInt("category_id"));
				item.setBrandId(rs.getInt("brand_id"));
				itemList.add(item);
			}
		} catch (SQLException e) {
			e.fillInStackTrace();
		} finally {
			 try {
                 con.close();
             } catch (SQLException e) {
                 e.printStackTrace();
             }
		}
		return itemList;
	}

	public static ArrayList<ItemDataBeans> getItemDataByCategoryId(int cid) {
		Connection con = null;
		PreparedStatement st = null;
		ArrayList<ItemDataBeans> idbList = new ArrayList<ItemDataBeans>();
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("SELECT * FROM item WHERE category_id = ?");
			st.setInt(1, cid);
			ResultSet rs = st.executeQuery();
			while (rs.next()) {
				ItemDataBeans idb = new ItemDataBeans();
				idb.setId(rs.getInt("id"));
				idb.setName(rs.getString("name"));
				idb.setDetail(rs.getString("detail"));
				idb.setPrice(rs.getInt("price"));
				idb.setFileName(rs.getString("file_name"));
				idb.setCategoryId(rs.getInt("category_id"));
				idb.setBrandId(rs.getInt("brand_id"));
				idbList.add(idb);
			}
		} catch(SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return idbList;
	}

	public static ArrayList<ItemDataBeans> getItemDataByBrandId(int bid) {
		Connection con = null;
		PreparedStatement st = null;
		ArrayList<ItemDataBeans> idbList = new ArrayList<ItemDataBeans>();
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("SELECT * FROM item WHERE brand_id = ?");
			st.setInt(1, bid);
			ResultSet rs = st.executeQuery();
			while (rs.next()) {
				ItemDataBeans idb = new ItemDataBeans();
				idb.setId(rs.getInt("id"));
				idb.setName(rs.getString("name"));
				idb.setDetail(rs.getString("detail"));
				idb.setPrice(rs.getInt("price"));
				idb.setFileName(rs.getString("file_name"));
				idb.setCategoryId(rs.getInt("category_id"));
				idb.setBrandId(rs.getInt("brand_id"));
				idbList.add(idb);
			}
		} catch(SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return idbList;
	}

	public static ItemDataBeans getItemDataBeansById(int id) {
		Connection con = null;
		PreparedStatement st = null;
		ItemDataBeans idb = new ItemDataBeans();
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("SELECT * FROM item WHERE id = ?");
			st.setInt(1, id);
			ResultSet rs = st.executeQuery();
			if (rs.next()) {
				idb.setId(rs.getInt("id"));
				idb.setName(rs.getString("name"));
				idb.setDetail(rs.getString("detail"));
				idb.setPrice(rs.getInt("price"));
				idb.setFileName(rs.getString("file_name"));
				idb.setCategoryId(rs.getInt("category_id"));
				idb.setBrandId(rs.getInt("brand_id"));
			}
		} catch(SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return idb;
	}


	public int createItem(String name, String detail, int price, String file, int brandId, int categoryId) {
		Connection conn = null;
		int autoIncKey = -1;
		try {
			conn = DBManager.getConnection();
			String sql = "INSERT INTO item(name, detail, price, file_name, brand_id, category_id) VALUES(?, ?, ?, ?, ?, ?)";
			PreparedStatement pStmt = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			pStmt.setString(1, name);
			pStmt.setString(2, detail);
			pStmt.setInt(3, price);
			pStmt.setString(4, file);
			pStmt.setInt(5, brandId);
			pStmt.setInt(6, categoryId);
			pStmt.executeUpdate();

			ResultSet rs = pStmt.getGeneratedKeys();
			if (rs.next()) {
				autoIncKey = rs.getInt(1);
			}


		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return autoIncKey;
	}

	public void createItem2 (int autoIncKey, int sizeId, int stock) {
		Connection conn = null;
		try {
			conn = DBManager.getConnection();
			String sql = "INSERT INTO item_size(item_id, size_id, stock) VALUES(?, ?, ?)";
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, autoIncKey);
			pStmt.setInt(2, sizeId);
			pStmt.setInt(3, stock);
			pStmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public static void deleteItemDataById(int id) {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("DELETE FROM item WHERE id = ?");
			st.setInt(1, id);
			st.executeUpdate();
		} catch(SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public static void deleteItemSizeDataById(int id) {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("DELETE FROM item_size WHERE item_id = ?");
			st.setInt(1, id);
			st.executeUpdate();
		} catch(SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}




}