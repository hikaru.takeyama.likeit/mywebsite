package ec;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemDataBeans;
import dao.ItemDAO;
import dao.ItemSizeDAO;

@WebServlet("/ItemAdd")
public class ItemAdd extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public ItemAdd() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();

		int id = Integer.parseInt(request.getParameter("id"));
		int sizeId = Integer.parseInt(request.getParameter("sizeId"));
		int stock = ItemSizeDAO.getStockByItemIdAndSizeId(id, sizeId);
		ItemDataBeans idb = ItemDAO.getItemDataBeansById(id);
		idb.setSizeId(sizeId);
		idb.setStock(stock);

		ArrayList<ItemDataBeans> cart = (ArrayList<ItemDataBeans>) session.getAttribute("cart");

		if (cart == null) {
			cart = new ArrayList<ItemDataBeans>();
		}

		cart.add(idb);




		session.setAttribute("cart", cart);

		response.sendRedirect("Cart");
	}

}
