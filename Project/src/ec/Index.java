package ec;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.BrandDataBeans;
import beans.CategoryDataBeans;
import beans.ItemDataBeans;
import dao.ItemDAO;

@WebServlet("/Index")
public class Index extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public Index() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String bid = request.getParameter("bid");
		String cid = request.getParameter("cid");

		ArrayList<BrandDataBeans> bdbList = ItemDAO.getBrandData();
		ArrayList<CategoryDataBeans> cdbList = ItemDAO.getCategoryData();
		request.setAttribute("bdbList", bdbList);
		request.setAttribute("cdbList", cdbList);



		ArrayList<ItemDataBeans> idbList = null;
		if(bid != null) {
			idbList = ItemDAO.getItemDataByBrandId(Integer.parseInt(bid));
		} else if(cid != null) {
			idbList = ItemDAO.getItemDataByCategoryId(Integer.parseInt(cid));
		} else {
			idbList = ItemDAO.getAllItemData();
		}

		request.setAttribute("idbList", idbList);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/index.jsp");
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}

}
