package ec;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.ItemDataBeans;
import beans.ItemSizeDataBeans;
import dao.ItemDAO;
import dao.ItemSizeDAO;

@WebServlet("/Item")
public class Item extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public Item() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String id = request.getParameter("id");
		ItemDataBeans idb = ItemDAO.getItemDataBeansById(Integer.parseInt(id));
		ArrayList<ItemSizeDataBeans> isdbList = ItemSizeDAO.getItemSizeDataById(Integer.parseInt(id));
		request.setAttribute("idb", idb);
		request.setAttribute("isdbList", isdbList);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/itemdetail.jsp");
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}

}
