package management;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import beans.BrandDataBeans;
import beans.CategoryDataBeans;
import beans.SizeDataBeans;
import dao.ItemDAO;

@WebServlet("/ItemCreate")
@MultipartConfig(location="C:\\Users\\LIKEIT_STUDENT.DESKTOP-38GIBV3.000\\Documents\\mywebsite\\Project\\WebContent\\img", maxFileSize=1048576)
public class ItemCreate extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public ItemCreate() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ArrayList<BrandDataBeans>brandList = ItemDAO.getBrandData();
		ArrayList<CategoryDataBeans>categoryList = ItemDAO.getCategoryData();
		ArrayList<SizeDataBeans>sizeList = ItemDAO.getSizeData();
		request.setAttribute("brandList", brandList);
		request.setAttribute("categoryList", categoryList);
		request.setAttribute("sizeList", sizeList);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/management/itemcreate.jsp");
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

//		リクエストパラメータ取得

		String name = request.getParameter("name");
		String detail = request.getParameter("detail");
		String stringPrice = request.getParameter("price");
		Part part = request.getPart("file");
		String brand = request.getParameter("brand");
		String category = request.getParameter("category");
		String[] size = request.getParameterValues("size");
		String stringStock = request.getParameter("stock");

		part.write(getFileName(part));

//		型変換

		int price = Integer.parseInt(stringPrice);
		int brandId = Integer.parseInt(brand);
		int categoryId = Integer.parseInt(category);
		int stock = Integer.parseInt(stringStock);

//		itemテーブルにINSERT

		ItemDAO itemDao = new ItemDAO();
		int autoIncKey = -1;
		autoIncKey = itemDao.createItem(name, detail, price, getFileName(part), brandId, categoryId);

//		item_sizeテーブルにINSERT

		for(String str:size) {
			itemDao.createItem2(autoIncKey, Integer.parseInt(str), stock);
		}

		response.sendRedirect("ItemManagement");
	}

	 private String getFileName(Part part) {
	        String name = null;
	        for (String dispotion : part.getHeader("Content-Disposition").split(";")) {
	            if (dispotion.trim().startsWith("filename")) {
	                name = dispotion.substring(dispotion.indexOf("=") + 1).replace("\"", "").trim();
	                name = name.substring(name.lastIndexOf("\\") + 1);
	                break;
	            }
	        }
	        return name;
	    }

}
