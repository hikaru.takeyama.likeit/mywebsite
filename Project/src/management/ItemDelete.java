package management;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.ItemDataBeans;
import dao.ItemDAO;

@WebServlet("/ItemDelete")
public class ItemDelete extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public ItemDelete() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String id = request.getParameter("id");
		ItemDataBeans idb = ItemDAO.getItemDataBeansById(Integer.parseInt(id));
		request.setAttribute("idb", idb);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/management/itemdelete.jsp");
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		String id = request.getParameter("id");
		ItemDAO.deleteItemDataById(Integer.parseInt(id));
		ItemDAO.deleteItemSizeDataById(Integer.parseInt(id));
		response.sendRedirect("ItemManagement");

	}

}
