create database myecsite_db default character set utf8;


create table user (
	id int primary key auto_increment,
	name varchar(255) not null,
	adress varchar(255) unique not null,
	login_id varchar(255) unique not null,
	password varchar(255) not null,
	create_date DATETIME DEFAULT CURRENT_TIMESTAMP

);

create table buy (
	id int primary key auto_increment,
	user_id int,
	total_price int,
	buy_date DATETIME DEFAULT CURRENT_TIMESTAMP
);

create table buy_detail (
	id int primary key auto_increment,
	buy_id int,
	item_id int
);

create table item  (
	id int primary key auto_increment,
	name varchar(255) not null,
	detail varchar(255),
	price int not null,
	file_name varchar(255),
	category_id int,
	brand_id int,
	create_date DATETIME DEFAULT CURRENT_TIMESTAMP
);
	
create table category (
	id int primary key auto_increment,
	name varchar(255)
);

create table brand (
	id int primary key auto_increment,
	name varchar(255)
);

create table item_size (
	id int primary key auto_increment,
	item_id int,
	size_id int,
	stock int
);

create table size (
	id int primary key auto_increment,
	size varchar(255)
);

insert into brand (name) values
('sacai'),('LEMAIRE'),('maison margiela'),('AURALEE'),('OAMC');

insert into category (name) values
('Tops'),('Bottoms'),('Accessories'),('Footwear');

insert into size (size) values
('44'),('46'),('48');
